#!/usr/bin/env python

from setuptools import setup, find_packages
from codecs import open
import os.path
import sys

here = os.path.abspath(os.path.dirname(__file__))

# Get the long description from the README file
with open(os.path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='docker-registry-py',
    version='0.2',
    author='David M Johnson',
    author_email='johnsond@flux.utah.edu',
    url='https://gitlab.flux.utah.edu/emulab/docker-registry-py',
    description='A Python library for the Docker Registry API.',
    long_description=long_description,
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Other Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Utilities',
        'License :: OSI Approved :: GNU Affero General Public License v3',
    ],
    keywords='docker dockerregistry',
    #package_dir={"": "."},
    packages=find_packages(where="."),
    #install_requires=['cryptography','jwt',],
    #extra_requires=[],
    entry_points={
        'console_scripts': [
            'docker-registry-cli=dockerreg.client.__main__:main'
        ],
    },
    #scripts=[ 'bin/pydockerauth-flask' ],
    #
    # Ok, for the same reason, we don't use console_scripts, we also
    # cannot use scripts.  Rather than implement our own version of
    # either, we just use data_files.  There are all kinds of reasons
    # not to do this, but it gets the job done for now, if you are
    # installing to the filesystem as a privileged user.  If you are
    # install --user, not so good for you!
    #
    #data_files=[(os.path.join(sys.exec_prefix,'bin'),['bin/pydockerauth-flask']),
    #            ('bin',['bin/pydockerauth-flask'])],
    #include_package_data=True,
)
