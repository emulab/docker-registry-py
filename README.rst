``docker-registry-py``
======================

.. image:: https://gitlab.flux.utah.edu/emulab/docker-registry-py/badges/master/build.svg
    :target: https://gitlab.flux.utah.edu/emulab/docker-registry-py
    :alt: Build Status

.. image:: https://gitlab.flux.utah.edu/emulab/dockerauth/badges/master/test.svg
    :target: https://gitlab.flux.utah.edu/emulab/dockerauth
    :alt: Test Status

``docker-registry-py`` (https://gitlab.flux.utah.edu/emulab/docker-registry-py)
is a simple Docker Registry v2 library that supports Python 2 and 3 on
Linux and FreeBSD (these are the only platforms tested thus far).  It
has partial v1 support, but that is a pointless effort at this point;
more just a PoC for the organization and future API version support.
