import logging

# Default to something sane for our package
LOG = logging.getLogger('dockerreg')
ENABLED = False

def logEnable(level):
    global ENABLED
    nh = logging.StreamHandler()
    fm = logging.Formatter(
        "%(asctime)s [%(process)d] %(levelname)-8s %(pathname)s"
        ":%(funcName)s:%(lineno)d %(message)s")
    nh.setFormatter(fm)
    LOG.addHandler(nh)
    for mod in ['requests']:
        ml = logging.getLogger(mod)
        ml.addHandler(nh)
        ml.setLevel(level)
    LOG.setLevel(logging.DEBUG)
    LOG.debug("logging enabled")
    ENABLED = True

def isEnabled():
    return ENABLED

def setLogger(new_logger):
    global LOG
    LOG.debug("switching from current logger %r to new logger %r"
                 % (repr(LOG),repr(new_logger)))
    LOG = new_logger

def getLogger():
    return LOG
