from dockerreg.models import Registry, Repository, Image, Manifest

class RegistryV1(Registry):

    def __init__(self,api,name=None):
        if api.version != 1:
            raise ex.ModelApiVersionMismatch("v1 registry, not v1 api")
        super(RegistryV1,self).__init__(api,name=name)

    def get_namespaces(self,regexp=None):
        """
        :param str regexp: a regexp to filter the namespaces.
        :returns: a :py:class:`list` of namespaces in this registry, possibly filtered by `regexp`.
        """

    def get_repositories(self,regexp=None):
        """
        :param str regexp: a regexp to filter the repositories
        :returns: a :py:class:`dict` of namespaces in this registry, possibly filtered by `regexp`.
        """

class RepositoryV1(Repository):
    pass

class ImageV1(Image):

    def get_layer(self):
        pass

    def put_layer(self):
        pass

    def get_layer_desc(self):
        pass

    def put_layer_desc(self):
        pass
        
    def ancestry(self):
        """
        :returns: the :py:class:`list` of layers that compose this image
        :rtype: :py:class:`list`
        """
        pass

class ManifestV1(Manifest):
    _media_types = ["application/vnd.docker.container.image.v1+json"]

    def __init__(self,api,blob={},raw=b'{}',media_type=None,repository=None,tag=None,id=None):
                 #name,id,parent,created,architecture,size,checksum,
                 #author=None,os=None,config=None):
        if not media_type:
            media_type = "application/vnd.docker.container.image.v1+json"
        super(ManifestV1,self).__init__(
            api,blob,raw,media_type=media_type,
            repository=repository,tag=tag,id=blob.get("id",None))
        self._version = 1

    def verify(self):
        return None

    @property
    def created(self):
        if "created" in self._attrs:
            return self._attrs["created"]
        return None

    @property
    def id(self):
        if "id" in self._attrs:
            return self._attrs["id"]
        return None

    @property
    def parent(self):
        if "parent" in self._attrs:
            return self._attrs["parent"]
        return None

    @property
    def container_config(self):
        if "container_config" in self._attrs:
            return self._attrs["container_config"]
        return None

    @property
    def config(self):
        if "config" in self._attrs:
            return self._attrs["config"]
        return None

    @property
    def os(self):
        if "os" in self._attrs:
            return self._attrs["os"]
        return None

    @property
    def architecture(self):
        if "architecture" in self._attrs:
            return self._attrs["architecture"]
        return None

    @property
    def container(self):
        if "container" in self._attrs:
            return self._attrs["container"]
        return None

    @property
    def docker_version(self):
        if "docker_version" in self._attrs:
            return self._attrs["docker_version"]
        return None

    @property
    def size(self):
        if "Size" in self._attrs:
            return self._attrs["Size"]
        return None

    def refresh(self):
        nattrs = self.api.get_image(self.id)
        if json.dumps(nattrs,indent=None,separators=(',',':')) \
          == json.dumps(self._attrs,indent=None,separators=(',',':')):
            return False
        self._attrs = nattrs
        return True
