from dockerreg.log import LOG

class DockerRegistryError(Exception):
    pass

class AuthStorageError(DockerRegistryError):
    pass

class BearerTokenError(DockerRegistryError):
    pass

class MissingCredentialsError(DockerRegistryError):
    pass

class BearerRedirectError(DockerRegistryError):
    pass

class UnsupportedAuthorizationTypeError(DockerRegistryError):
    pass

class UnsupportedRegistryVersionError(DockerRegistryError):
    pass

class ModelApiVersionMismatch(DockerRegistryError):
    pass

class IllegalArgumentError(DockerRegistryError):
    pass

class UnknownManifestFormat(DockerRegistryError):
    pass

class MalformedManifestError(DockerRegistryError):
    pass

class MalformedManifestListError(DockerRegistryError):
    pass

class LayerContentMismatch(DockerRegistryError):
    pass

class DockerRegistryAPIError(DockerRegistryError):
    def __init__(self,httpcode,errors=[]):
        LOG.debug("%d %s" % (httpcode,str(errors)))
        self._httpcode = httpcode
        if errors is None:
            errors = []
        self._errors = errors

    @property
    def count(self):
        return len(self._errors)

    @property
    def httpcode(self):
        return self._httpcode

    def code(self,idx=0):
        if idx >= self.count or not "code" in self._errors[idx]:
            return None
        return self._errors[idx]["code"]

    def message(self,idx=0):
        if idx >= self.count or not "message" in self._errors[idx]:
            return None
        return self._errors[idx]["message"]

    def detail(self,idx=0):
        if idx >= self.count or not "detail" in self._errors[idx]:
            return None
        return self._errors[idx]["detail"]

    def __repr__(self):
        if self.count == 1:
            return "<%s:HTTP %s,%s,%s,%r>" % (
                self.__class__.__name__,self._httpcode,
                self.code(),self.message(),repr(self.detail()))
        else:
            tmp = ["[%s,%s,%r]" \
                      % (x["code"],x["message"],repr(x["detail"])) for x in self._errors]
            return "<%s:HTTP %s,%s,[%s]>" % (
                self.__class__.__name__,self._httpcode,tmp.join(","))

class AuthorizationRequiredError(DockerRegistryAPIError):
    def __init__(self,errors=[]):
        if not errors:
            errors = [{"code":401,"message":"Authorization required"}]
        super(AuthorizationRequiredError,self).__init__(401,errors=errors)

class ImageNotFoundError(DockerRegistryAPIError):
    def __init__(self,errors=[]):
        if not errors:
            errors = [{"code":404,"message":"Image not found"}]
        super(ImageNotFoundError,self).__init__(404,errors=errors)

class TagNotFoundError(DockerRegistryAPIError):
    def __init__(self,errors=[]):
        if not errors:
            errors = [{"code":404,"message":"Tag not found"}]
        super(TagNotFoundError,self).__init__(404,errors=errors)

class LayerNotFoundError(DockerRegistryAPIError):
    def __init__(self,errors=[]):
        if not errors:
            errors = [{"code":404,"message":"Layer not found"}]
        super(LayerNotFoundError,self).__init__(404,errors=errors)

class RepositoryNotFoundError(DockerRegistryAPIError):
    def __init__(self,errors=[]):
        if not errors:
            errors = [{"code":404,"message":"Repository not found"}]
        super(RepositoryNotFoundError,self).__init__(404,errors=errors)
