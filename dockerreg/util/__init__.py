import base64
import time
from dockerreg.log import LOG

def utcoffset():
    """
    Return the offset from local time to UTC time, in seconds.
    """
    if time.daylight:
        return time.altzone
    else:
        return time.timezone

def parse_bool(str):
    if str is None:
        return
    str = str.lstrip(' ').rstrip(' ')
    if str in ['True','TRUE','true','1','yes','YES','Yes']:
        return Truef
    return False

def parse_bool_or_int(str):
    if str is None:
        return
    str = str.lstrip(' ').rstrip(' ')
    if str in ['True','TRUE','true','T','t','yes','YES','Yes','Y','y']:
        return True
    elif str in ['False','FALSE','false','F','f','no','NO','No','N','n']:
        return False
    else:
        return int(str)
    pass

def parse_str_list(str):
    if str is None:
        return
    str = str.lstrip(' ').rstrip(' ')
    if str == '':
        return []
    return str.split(',')

def parse_rspec_arg(_rspec):
    try:
        from lxml import etree as ET
        ET.fromstring(_rspec)
        return _rspec
    except:
        pass
    try:
        f = open(_rspec,'r')
        return f.read()
    except:
        LOG.warn("rspec argument does not appear to be XML nor a file;"
                 " but assuming it is indeed XML!")
        return _rspec
    pass
    
def parse_safe_eval(value):
    retval = None
    try:
        retval = eval(value)
        return retval
    except:
        LOG.exception("Argument must be Python code")
        raise Exception("Argument must be Python code")
    pass
    
def parse_safe_list_or_eval(value):
    retval = None
    try:
        retval = eval(value)
        return retval
    except:
        pass
    try:
        _list = value.split(',')
        return _list
    except:
        pass
    raise Exception("Argument must be Python code, or a list of strings")

class UnrecognizedSystemError(Exception):
    pass

class UnrecognizedArchitectureError(Exception):
    pass

P2G_ARCH = { "i386":"386","x86_64":"amd64","aarch64":"arm64" }
def getgoarch():
    import platform
    arch = platform.architecture()
    for x in [ arch,arch.lower() ]:
        if x in list(P2G_ARCH.values()):
            return x
        if x in P2G_ARCH:
            return P2G_ARCH[x]
    raise UnrecognizedArchitecture(arch)

P2G_OS = { "FreeBSD":"freebsd","Linux":"linux","Windows":"windows" }
def getgoos():
    import platform
    os = platform.system()
    for x in [ os,os.lower() ]:
        if x in list(P2G_OS.values()):
            return x
        if x in P2G_OS:
            return P2G_OS[x]
    raise UnrecognizedSystem(os)

class IllegalJOSEBase64URLString(Exception):
    pass

def joseb64urlencode(s):
    return base64.urlsafe_b64encode(s).rstrip(b'=')

def joseb64urldecode(s):
    s = s.replace("\n","").replace(" ","")
    lm = len(s) % 4
    if lm == 2:
        s += "=="
    elif lm == 3:
        s += "="
    elif lm == 0:
        pass
    else:
        raise IllegalJOSEBase64URLString()
    return base64.urlsafe_b64decode(s.encode('utf-8'))

def num_joseb64urlencode(n):
    b = bytearray()
    while n:
        b.insert(0,n & 0xff)
        n >>= 8
    # Pad to 32 bytes.
    for i in range(len(b),32):
        b.insert(0,0)
    return joseb64urlencode(b)

def str2bytes(maybe_str):
    if not isinstance(maybe_str,bytes) and isinstance(maybe_str,str):
        maybe_str = maybe_str.encode()
    return maybe_str

def bytes2str(maybe_bytes):
    if isinstance(maybe_bytes,bytes) and not isinstance(maybe_bytes,str):
        maybe_bytes = maybe_bytes.decode()
    return maybe_bytes
