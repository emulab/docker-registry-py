import sys
import argparse
import json
import logging

from dockerreg.log import LOG, logEnable
from dockerreg.auth import DockerConfigAuth,IndexedBasicAuth,BearerTokenCache
from dockerreg.client import ApiClient #RegistryClient
from dockerreg.util.applicable import Applicable
import dockerreg.exceptions as ex

class DefaultSubcommandArgumentParser(argparse.ArgumentParser):
    __default_subparser = None

    def set_default_subparser(self, name):
        self.__default_subparser = name

    def _parse_known_args(self, arg_strings, *args, **kwargs):
        in_args = set(arg_strings)
        dsp = self.__default_subparser
        if dsp is not None and not {'-h', '--help'}.intersection(in_args):
            for x in self._subparsers._actions:
                if isinstance(x, argparse._SubParsersAction) \
                  and in_args.intersection(list(x._name_parser_map.keys())):
                    break
        else:
            # insert default in first position, this implies no
            # global options without a sub_parsers specified
            arg_strings = [dsp] + arg_strings
        return super(DefaultSubcommandArgumentParser,self)._parse_known_args(
            arg_strings, *args, **kwargs)

def parse_options():
    #parser = DefaultSubcommandArgumentParser()
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--debug",dest="debug",action='store_true',
                        help="Enable debugging log level")
    parser.add_argument("-s","--server",dest="server",required=True,
                        help="The registry server host:port string")
    parser.add_argument("-S","--source-address",dest="source_address",
                        default=None,
                        help="The source IP address to use.")
    parser.add_argument("--server-version",dest="server_version",
                        type=int,default=None,
                        help="The registry server host:port string")
    parser.add_argument("-u","--username",dest="username",default=None,
                        help="The username to use for all authentication to this registry")
    parser.add_argument("-p","--password",dest="password",default=None,
                        help="The password to use for all authentication to this registry")
    parser.add_argument("-C","--cred-file",dest="cred_file",default=None,
                        type=argparse.FileType('r'),
                        help="A file containing <username>:<password> on a single line.")
    
    parser.add_argument("--skip-docker-config-auth",dest="skip_docker_config",
                        action="store_true",
                        help="Do not load the Docker config file in search of authentication information")
    parser.add_argument("--cert",dest="cert",default=None,
                        type=argparse.FileType('r'),
                        help="Set the client certificate to use")
    parser.add_argument("--key",dest="key",default=None,
                        type=argparse.FileType('r'),
                        help="Set the client private key to use")
    parser.add_argument("--ca-bundle",dest="ca_bundle",default=None,
                        type=argparse.FileType('r'),
                        help="Set the CA bundle used to verify the server")
    parser.add_argument("--no-verify",action="store_true",
                        help="Do not verify the server's SSL certificate")
    parser.add_argument("--no-cache",dest="no_cache",action="store_true",
                        help="Do not save authentication tokens in the cache file.")
    parser.add_argument("--cache-file",dest="cache_file",default=None,
                        type=argparse.FileType('rw'),
                        help="Use this cache file instead of the default (~/.dockerreg.cache")
    # Add in subparsers.
    subparsers = parser.add_subparsers(help="Subcommands",dest='subcommand')
    #subparsers.add_parser('interactive',help="Run in interactive mode")
    Applicable.add_subparsers(subparsers)
    #parser.set_default_subparser('interactive')

    (options, args) = parser.parse_known_args(sys.argv[1:])
    return (options, args)

def main():
    (options, args) = parse_options()
    #if options.no_verify and options.ca_bundle is not None:
    #    raise argparse.ArgumentError("cannot specify both a cabundle to verify the server, and then request --no-verify!")

    if options.debug:
        logEnable(logging.DEBUG)

    verify_val = True
    if options.no_verify:
        verify_val = False
    elif options.ca_bundle is not None:
        verify_val = options.ca_bundle
    if options.cred_file:
        try:
            [u,p] = options.cred_file.readline().rstrip('\n').split(":",1)
            options.cred_file.close()
        except:
            sys.stderr.write("ERROR: failed to read creds file %s, aborting!\n"
                             % (options.cred_file.name))
            return -1
    else:
        [u,p] = [options.username,options.password]
    if u == '' \
      or (u == None and options.skip_docker_config) \
      or (u == None and not DockerConfigAuth.configfile_exists()):
        auth = IndexedBasicAuth(dict())
    elif not u and not options.skip_docker_config:
        auth = DockerConfigAuth()
    else:
        auth = IndexedBasicAuth.from_user_pass(
            options.server,u,password=p,
            skip_docker_config=options.skip_docker_config)
    cache = None
    if not options.no_cache:
        if options.cache_file:
            cache = BearerTokenCache(filename=options.cache_file)
        else:
            cache = BearerTokenCache()
        LOG.debug("cache contents:\n%s" % (cache.format_dump()))
    certfilename = None
    if options.cert:
        # requests.Session requires a filename, not a file
        certfilename = options.cert.name
    keyfilename = None
    if options.key:
        keyfilename = options.key.name
    srcaddr = None
    if options.source_address:
        srcaddr = (options.source_address,0)
    api = ApiClient( #Registry( #RegistryClient(
        options.server,version=options.server_version,
        username=u,auth=auth,verify=verify_val,
        cert=certfilename,key=keyfilename,
        cache=cache,source_address=srcaddr)
    Applicable.register_object(api)
    registry = api.registry()
    Applicable.register_object(registry)
    try:
        retval = Applicable.apply(options.subcommand,options)
        #if type(retval) in [str,int,float]:
        sys.stdout.write("%s\n" % (str(retval)))
        #else:
        #    print json.dumps(retval,indent=2)
    except ex.DockerRegistryAPIError:
        (typ,v,tb) = sys.exc_info()
        #print >>sys.stderr, "%r" % (repr(v))
        sys.stderr.write(
          "%s: HTTP %d: %s: %s (%s)\n" % (v.__class__.__name__,v.httpcode,v.code(),v.message(),v.detail())
          )
        sys.exit(v.httpcode)
    if cache:
        cache.save()

if __name__ == "__main__":
    main()
