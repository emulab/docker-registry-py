Installation
============

You can install `docker-registry-py` in the normal way:

    ``$ python setup.py install --user``

or

    ``$ python setup.py install --prefix=/usr/local``

or similar.  If you use the ``--user`` option, `docker-registry-py` will be
installed in ``$HOME/.local/{bin,lib}`` or thereabouts.

You can build the `docker-registry-py` documentation via

    ``$ python setup.py build_sphinx``

The documentation will be available in `docs/build/html`.
