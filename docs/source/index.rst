.. docker-registry-py documentation master file, created by
   sphinx-quickstart on Mon Aug 14 16:45:58 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../../README.rst

Contents:

.. toctree::
   :maxdepth: 2

   install
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

